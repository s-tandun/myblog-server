const server = require('../server.js');
const PostModel = require('../models/postModel.js');

/* Initialise posts table with random article from dummyjson.com/post API */

async function fetchData(url) {
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const posts = [];
        for (var i=0; i<11; i++) {
          const re = /(.*[.!?]\s){2}/;
          const text = data.posts[i].body;
          const match = text.match(re);
          const summary = match ? match[0] : "";

            const post = {
                "title": data.posts[i].title,
                "summary": summary,
                "content": data.posts[i].body,
                "datetime": "May 3 2024",
                "likes": data.posts[i].reactions.likes,
                "comments": data.posts[i].reactions.dislikes,
                "file": "sunglass.png"
            }
            posts.push(post);
        }
        console.log(posts);

        await PostModel.deleteMany({});
        const res = await PostModel.insertMany(posts);
        console.log(res);
    } else {
        throw new Error('Failed to fetch data');
        }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

function main() {
    const apiUrl = 'https://dummyjson.com/posts'; 
    fetchData(apiUrl);
}

// Call the main function to start execution
main();