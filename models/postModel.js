const mongoose = require('mongoose');

const postSchema = mongoose.Schema(
    {
        title: {
            type: String,
            require: [true, "Please provide a title"]
        },
        summary: {
            type: String,
            require: [true, "Please provide a summary"]
        },
        author: {
            type: String,
            require: [true, "Please provide a summary"]
        },
        content: {
            type: String,
            require: [true, "Please provide a content"]
        },
        datetime: {
            type: String
        },
        likes: {
            type: String
        },
        comments: {
            type: String
        },
        file: {
            type:String
        }
    }
)

const Post = mongoose.model('Post', postSchema);

module.exports = Post;