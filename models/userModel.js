const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
    {
        username: {
            type: String,
            require: [true, "Please provide a username"]
        },
        password: {
            type: String,
            require: [true, "Please provide a password"]
        }
    }
)

const User = mongoose.model('User', userSchema);

module.exports = User;