const express = require('express');
const routes = require('../routes');
const server = require('../server');
const cors = require('cors');
const serverless = require("serverless-http");
const app = express();

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use("/", routes);

module.exports.handler = serverless(app);