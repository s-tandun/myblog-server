ARG NODE_VERSION=23.0.0

FROM node:${NODE_VERSION}-alpine

# Use production node environment by default.
ENV NODE_ENV=production

WORKDIR /app

# Copy package.json and package-lock.json
COPY package.json package-lock.json ./

# Download the dependencies
RUN npm install --production

# Copy the rest of the source files into the image.
COPY . .

# Run the application as a non-root user
USER node

# Export the port that the application listens on.
EXPOSE 3000

# Run the application.
CMD ["node", "app.js"]