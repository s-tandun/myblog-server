# Blog App Backend

Backend services for a blog app built using NodeJS, it provides database access and user registration and login authentication. Blog articles and user data are stored in MongoDB, which can be pulled and sent to the client. The articles are fetched from DummyJSON and added to the posts database.

Live server: https://blog-server2025.netlify.app/test

## Functionalities

- `/test`: for debugging, returns "Hello world"
- `/posts`: retrieves all blog posts.
- `/post/:id`: retrieves a single blog post by id.
- `/login`: accepts a username and password, and checks if the user exists in the database.
- `/sign-up`: registers a new user to the database.
- `/send-email`: sends an email.

## Goals

- Practise server-side logic using JavaScript
- Connect to MongoDB database, create a schema and perform CRUD operations with Mongoose
- Use Express.js to define route and HTTP GET and POST methods
- Use Git CI to automate deployment on Netlify whenever changes are made to the repo.

## Lessons Learned

Implemented password hashing using bcrypt to ensure sensitive information (such as passwords) is not stored in the database, if anyone would have access to it.

## How to run

### Install all dependencies listed in package.json.

```sh
npm install
```

### Start the application on localhost.

```sh
npm run start
```

## Resources

Dummy posts - https://dummyjson.com/posts
