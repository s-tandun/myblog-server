/* Initialise mongoose schema and set up connection to mongodb */

const mongoose = require('mongoose');
const uri = process.env.MONGODB_URI;

const dbname = "blog";

const connectToDatabase = async () => {
    try {
        await mongoose.connect(uri);
        console.log(`Connected to MongoDB`);

        //mongoose.connection.useDb(dbname);
        //console.log(`Switch to the ${dbname} database`)
    } catch (err) {
        console.log(`Error connecting to the database: ${err}`)
    }
};

const main = async () => {
    try {
        await connectToDatabase();
    } catch (err) {
        console.error( `Error: ${err}`)
    }
};

main();