const express = require('express');
const mongoose = require('mongoose');
const PostModel = require('./models/postModel');
const UserModel = require('./models/userModel');
const sgMail = require('@sendgrid/mail');
const bcrypt = require('bcryptjs'); /* password-hashing function */
require('dotenv').config(); /* npm module dotenv to load values from .env file */

const sendgridApiKey = process.env.SENDGRID_API_KEY;
const myEmailID = process.env.EMAIL_ADDRESS;

const router = express.Router();

/*
POST /api/posts           - Create a new post
GET  /api/posts           - Retrieve all posts
GET  /api/posts/:id       - Retrieve a post
POST  /api/register       - Create a new user
POST  /api/login          - Retrieve a user (user details sent via HTTP body rather than the URL)
POST /api/send-email      - Send an email
*/

router.get('/test', async(req,res) => {
   res.send("hello world");
});

// Insert a new post
router.post('/posts', async(req,res) => {
    try {
        console.log('add post');
        const post = await PostModel.create(req.body);
        post.save();
        res.send(post);
    } catch (error) {
        console.error(error);
        res.status(500).send(error.message);
    }
});

// Retrieve all posts
router.get('/posts', async(req,res) => {
    try {
        const index = req.query.index;
        var posts = [];
        if (index == 1) {
            posts = await PostModel.find({}).limit(3);
        } else {
            const offset = (index - 1) * 3;
            posts = await PostModel.find({}).skip(offset).limit(3);
        }
        res.json(posts);
    } catch(error) {
        console.error(error);
        res.status(500).send(error.message);
    }
});

// Retrieve a post
router.get('/posts/:id', async(req,res) => {
    try {
        const postId = req.params.id;
        const objectId = new mongoose.Types.ObjectId(postId);
        const post = await PostModel.findOne({_id: objectId});
        res.json(post);
        res.status(200);
    } catch(error) {
        console.error(error);
        res.status(500).send(error.message);
    }
});

// Create a new user
router.post('/register', async(req,res) => {
    try {
        const hashedPass = await bcrypt.hash(req.body.password, 10);
        const userDetails = {username: req.body.username, password: hashedPass};
        const newUser = await UserModel.create(userDetails);
        newUser.save();
        res.send(newUser);
        res.status(200).send();
    } catch(error) {
        console.log(error);
        res.status(500).send(`Error:${error}`);
    }
});

// Find a user (and compare password)
router.post('/login', async(req,res) => {

    const user = await UserModel.findOne({username: req.body.username});
    if (!user) { res.status(401).send(`Cannot find user ${req.body.username}`); }
    try {        
        const match = await bcrypt.compare(req.body.password, user.password);
        if (match) {
            res.status(200).send('Login success')
        } else { 
            res.status(500).send('Invalid credential')
        }
    } catch(error) {
        console.error(error);
        res.status(500).send(`Error:${error}`);
    }
})

// Send an email
router.post('/send-email', async(req,res) => {
    const { name, email, mailSubject, message } = req.body;
    sgMail.setApiKey(sendgridApiKey);

    const mail = {
        to: myEmailID,
        from: myEmailID,
        subject: mailSubject,
        text: `Name: ${name}\nEmail: ${email}\nMessage: ${message}`
    };

    try {
        await sgMail.send(mail).then(() => {
            res.status(200).send('Email sent successfully');
        })
    } catch(error) {
        console.error(error);
        res.status(500).send(`Error:${error}`);
    }
});

module.exports = router;