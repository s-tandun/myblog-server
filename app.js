const express = require('express');
const routes = require('./routes');
const server = require('./server');
const cors = require('cors');
const serverless = require("serverless-http");
const app = express();

app.use(cors());

/*
app.listen(3000, () => {
    console.log('Server started on port 3000'); // run locally
})
*/

app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use("/", routes);

export const handler = serverless(app); 